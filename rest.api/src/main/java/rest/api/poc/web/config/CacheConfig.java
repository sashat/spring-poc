package rest.api.poc.web.config;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Configuration
@EnableCaching
@PropertySource({"classpath:config.properties"})
public class CacheConfig {

    @Autowired
    private Environment env;
    
   @Bean
    RedisConnectionFactory redisConnectionFactory(){
        RedisStandaloneConfiguration redisStandaloneConfiguration = 
                new RedisStandaloneConfiguration(env.getProperty("spring.redis.host"), Integer.parseInt(env.getProperty("spring.redis.port")));
        
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(redisStandaloneConfiguration);
        
        return jedisConnectionFactory;
    }
    
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory){
        Map<String, RedisCacheConfiguration> cacheConfigurations = new HashMap<>();
        
        RedisCacheConfiguration cacheConf = RedisCacheConfiguration
                .defaultCacheConfig()
                .disableCachingNullValues()
                .prefixKeysWith("books")
                .entryTtl(Duration.ofSeconds(300));
        
        cacheConfigurations.put("books", cacheConf);
        
        RedisCacheManager cacheManager = RedisCacheManager
                .builder(redisConnectionFactory)
                .withInitialCacheConfigurations(cacheConfigurations)
                .build();
        
        return cacheManager;
    }
}
