package rest.api.poc.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rest.api.poc.dtos.SignUpDto;
import rest.api.poc.dtos.UserDto;
import rest.api.poc.services.IUserService;

@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private IUserService userService;
    
    @RequestMapping(value="sign-up", method = RequestMethod.POST)
    public UserDto signUp(@RequestBody SignUpDto dto) {
        return userService.signUp(dto.getUserName(), dto.getPassword());
    }
}
