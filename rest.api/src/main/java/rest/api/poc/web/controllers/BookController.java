package rest.api.poc.web.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RestController;
import rest.api.poc.dtos.BookDto;
import rest.api.poc.services.IBookService;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    IBookService bookService;
    
    @PreAuthorize("hasAuthority('read_book')")
    @RequestMapping(method = GET)
    public List<BookDto> findAll() {
        return bookService.searchByCriteria();
    }
    
    @PreAuthorize("hasAuthority('add_book')")
    @RequestMapping(method = POST)
    public BookDto create(@RequestBody BookDto book) {
        return bookService.create(book.getName(),book.getPublishYear(), book.getPageCount());
    }
}
