package rest.api.poc.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rest.api.poc.dtos.SignUpDto;
import rest.api.poc.dtos.UserDto;
import rest.api.poc.services.IUserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
    
    @Autowired
    private IUserService userService;
    
}
