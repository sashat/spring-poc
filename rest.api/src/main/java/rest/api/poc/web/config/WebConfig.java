
package rest.api.poc.web.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import rest.api.poc.repositories.BookRepository;
import rest.api.poc.repositories.IBookRepository;
import rest.api.poc.repositories.IUserRepository;
import rest.api.poc.repositories.UserRepository;
import rest.api.poc.services.BookService;
import rest.api.poc.services.IBookService;
import rest.api.poc.services.IUserService;
import rest.api.poc.services.OAuthUserDetailsService;
import rest.api.poc.services.UserService;

@Configuration
public class WebConfig {
    @Bean
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
    
    
    @Bean
    public UserDetailsService getUserDetailsService(){
        return new OAuthUserDetailsService();
    }
    //services
    @Bean
    public IUserService getUserService(){
        return new UserService();
    }
    
    @Bean
    public IBookService getBookService(){
        return new BookService();
    }
    
    //repositories
    
    @Bean
    public UserRepository getUserRepository() {
        return new UserRepository();
    }
    
    @Bean
    public BookRepository getIBookRepository() {
        return new BookRepository();
    }
}
