package rest.api.poc.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {
    
    @Bean
    public PasswordEncoder userPasswordEncoder(){
        return new BCryptPasswordEncoder(8);
    }
    
    @Bean
    public PasswordEncoder oAuthClientPasswordEncoder(){
        return new BCryptPasswordEncoder(4);
    }
}
