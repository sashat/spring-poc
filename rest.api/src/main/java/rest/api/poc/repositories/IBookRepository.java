package rest.api.poc.repositories;

import java.util.List;
import rest.api.poc.domain.Book;


public interface IBookRepository {
    List<Book> findByYear(int year);
}
