package rest.api.poc.repositories;

import java.util.List;
import rest.api.poc.domain.BaseEntity;

public interface IBaseRepository<TEntity extends BaseEntity> {
    TEntity getById();
    
    List<TEntity> getAll();
    
    TEntity createOrUpdate(TEntity entity);
    
    void delete(TEntity entity);
    
    void deleteById(int id);
}
