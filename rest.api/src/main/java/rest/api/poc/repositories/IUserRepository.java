package rest.api.poc.repositories;

import rest.api.poc.domain.User;

public interface IUserRepository {
    User getUserByUserName(String userName);
}
