package rest.api.poc.repositories;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import rest.api.poc.domain.BaseEntity;


public abstract class BaseRepository<TEntity extends BaseEntity> implements IBaseRepository<TEntity>{

    @Autowired
    protected EntityManager entityManager;
    
    //from https://developer.jboss.org/wiki/GenericDataAccessObjects?_sscc=t
    private final Class<TEntity> persistentClass = (Class<TEntity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    
    @Override
    public TEntity getById() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TEntity> getAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TEntity> query = criteriaBuilder.createQuery(persistentClass);
        Root<TEntity> from = query.from(persistentClass);
        query.select(from);
        
        List<TEntity> result = (List<TEntity>) entityManager
                .createQuery(query)
                .getResultList();
        
        return result;
    }

    @Override
    public TEntity createOrUpdate(TEntity entity) {
        try {
            entityManager.getTransaction().begin();
            
            entityManager.persist(entity);
            
            entityManager.getTransaction().commit();
        }
        catch(Exception ex) {
            entityManager.getTransaction().rollback();
            
            throw ex;
        }
        
        return entity;
    }

    @Override
    public void delete(BaseEntity entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
