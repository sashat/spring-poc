package rest.api.poc.repositories;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import rest.api.poc.domain.User;

@Repository
public class UserRepository extends BaseRepository<User> implements IUserRepository{

    @Override
    public User getUserByUserName(String userName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> from = query.from(User.class);
        query.select(from);
        query.where(criteriaBuilder.equal(from.get("userName"), userName));
        
        return  entityManager
                .createQuery(query)
                .getSingleResult();
    }
    

}
