package rest.api.poc.authorities;


public class AuthoritiesLU {
    public static final String BOOKS_READ = "read_book";
    public static final String BOOKS_ADD = "add_book";
    public static final String USERS_UPDATE = "update_user";
}
