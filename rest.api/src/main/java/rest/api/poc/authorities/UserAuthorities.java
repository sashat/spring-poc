package rest.api.poc.authorities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserAuthorities {
    public static Map<Integer, List<UserAuthority>> userAuthorities;
    
    private static boolean  isInitialized = false;
    
    public static void init(){
        Map<String, UserAuthority> authorities = new HashMap<>();
        
        authorities.put(AuthoritiesLU.BOOKS_ADD, new UserAuthority(AuthoritiesLU.BOOKS_ADD));
        authorities.put(AuthoritiesLU.BOOKS_READ, new UserAuthority(AuthoritiesLU.BOOKS_READ));
        authorities.put(AuthoritiesLU.USERS_UPDATE, new UserAuthority(AuthoritiesLU.USERS_UPDATE));
        
        List<UserAuthority> readerAuthorities = new ArrayList<>();
        readerAuthorities.add(authorities.get(AuthoritiesLU.BOOKS_READ));
        
        List<UserAuthority> authorAuthorities = new ArrayList<>();
        authorAuthorities.add(authorities.get(AuthoritiesLU.BOOKS_READ));
        authorAuthorities.add(authorities.get(AuthoritiesLU.BOOKS_ADD));
        
        List<UserAuthority> adminAuthorities = new ArrayList<>();
        adminAuthorities.add(authorities.get(AuthoritiesLU.BOOKS_READ));
        adminAuthorities.add(authorities.get(AuthoritiesLU.BOOKS_ADD));
        adminAuthorities.add(authorities.get(AuthoritiesLU.USERS_UPDATE));
        
        userAuthorities = new HashMap<>();
        userAuthorities.put(UserRoleLU.ADMIN, adminAuthorities);
        userAuthorities.put(UserRoleLU.AUTHOR, authorAuthorities);
        userAuthorities.put(UserRoleLU.READER, readerAuthorities);
        
        isInitialized = true;
    }
    
    
    public static List<UserAuthority> getByUserRole(int userRole){
        if (!isInitialized) {
            init();
        }
        return userAuthorities.get(userRole);
    }
}
