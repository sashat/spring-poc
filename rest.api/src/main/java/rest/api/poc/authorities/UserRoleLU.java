package rest.api.poc.authorities;


public class UserRoleLU {
    public static final int READER = 1;
    public static final int AUTHOR = 2;
    public static final int ADMIN = 3;
}
