package rest.api.poc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rest.api.poc.authorities.UserRoleLU;
import rest.api.poc.domain.User;
import rest.api.poc.dtos.UserDto;
import rest.api.poc.repositories.UserRepository;

@Service
public class UserService extends BaseService<User, UserDto, UserRepository> implements IUserService {

    @Autowired
    private PasswordEncoder userPasswordEncoder;
    
    @Override
    public UserDto signUp(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(userPasswordEncoder.encode(password));
        user.setRoleId(UserRoleLU.READER);
        
        user = repository.createOrUpdate(user);
        
        UserDto result = convertToDto(user);
        
        return result;
    }
    
}
