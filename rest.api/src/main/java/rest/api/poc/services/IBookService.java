package rest.api.poc.services;

import java.util.List;
import rest.api.poc.dtos.BookDto;

public interface IBookService {
    List<BookDto> searchByCriteria();
    
    BookDto create(String name, int year, int pageCount);
}
