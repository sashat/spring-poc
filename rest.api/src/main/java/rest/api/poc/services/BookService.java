package rest.api.poc.services;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import rest.api.poc.domain.Book;
import rest.api.poc.dtos.BookDto;
import rest.api.poc.repositories.BookRepository;

@Service
public class BookService extends BaseService<Book, BookDto, BookRepository> implements IBookService {

    @Override
    @Cacheable("books")
    public List<BookDto> searchByCriteria() {
        List<Book> dbBooks = repository.getAll();
        
        return dbBooks.stream()
                .map((book)->convertToDto(book))
                .collect(Collectors.toList());
    }

    @Override
    @CacheEvict(value="books", allEntries = true)
    public BookDto create(String name, int year, int pageCount) {
        BookDto book = new BookDto();
        book.setName(name);
        book.setPageCount(pageCount);
        book.setPublishYear(year);
        
        return create(book);
    }
    
}
