package rest.api.poc.services;

import rest.api.poc.domain.BaseEntity;
import rest.api.poc.dtos.BaseDto;

public interface IBaseService<TEntity extends BaseEntity, TDto extends BaseDto> {
    TDto create(TDto dto);
    TDto update(TDto dto);
    void delete(TDto dto);
    void deleteById(int id);
}
