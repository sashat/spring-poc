package rest.api.poc.services;

import rest.api.poc.dtos.UserDto;

public interface IUserService {
    UserDto signUp(String username, String password);
}
