package rest.api.poc.services;

import java.lang.reflect.ParameterizedType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import rest.api.poc.domain.BaseEntity;
import rest.api.poc.dtos.BaseDto;
import rest.api.poc.repositories.IBaseRepository;


public class BaseService<TEntity extends BaseEntity, TDto extends BaseDto, TRepository extends IBaseRepository<TEntity>> implements IBaseService<TEntity, TDto>{

    //from https://developer.jboss.org/wiki/GenericDataAccessObjects?_sscc=t
    private final Class<TDto> persistentDtoClass = (Class<TDto>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    
    private final Class<TEntity> persistentClass = (Class<TEntity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    
    @Autowired
    protected TRepository repository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Override
    public TDto create(TDto dto) {
        
        TEntity entity = convertToEntity(dto);
        
        TEntity result = repository.createOrUpdate(entity);
        
        return convertToDto(result);
    }

    @Override
    public TDto update(TDto dto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(TDto dto) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    protected TDto convertToDto(TEntity entity){
        return modelMapper.map(entity, persistentDtoClass);
    }
    
    protected TEntity convertToEntity(TDto dto){
        return modelMapper.map(dto, persistentClass);
    }

}
