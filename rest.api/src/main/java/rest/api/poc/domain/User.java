package rest.api.poc.domain;

import java.util.Collection;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import rest.api.poc.authorities.UserAuthorities;
import rest.api.poc.authorities.UserAuthority;

@Entity
@Table(name="user")
public class User extends BaseEntity implements UserDetails{
    
    @Column(name = "userName", length = 128)
    private String userName;
    
    @Column(name = "password", length = 255)
    private String password;
    
    @Column(name = "roleId")
    private int roleId;

    @Override
    public String getUsername() {
        return userName;
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<UserAuthority> authorities = UserAuthorities.getByUserRole(roleId);
        
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        //TODO: get rid of hardcode
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        //TODO: get rid of hardcode
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //TODO: get rid of hardcode
        return true;
    }

    @Override
    public boolean isEnabled() {
        //TODO: get rid of hardcode
        return true;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    
    
}
