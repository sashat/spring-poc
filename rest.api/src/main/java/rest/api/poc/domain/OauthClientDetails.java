package rest.api.poc.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oauth_client_details")
public class OauthClientDetails implements Serializable {
    @Id
    @Column(name = "client_id", length = 255)
    private String clientId;
    
    @Column(name = "resource_ids", length = 255)
    private String resourceIds;
    
    @Column(name = "client_secret", length = 255)
    private String clientSecret;
    
    @Column(name = "scope", length = 255)
    private String scope;
    
    @Column(name = "authorized_grant_types", length = 255)
    private String AuthorizedGrantTypes;
  
    @Column(name = "web_server_redirect_uri", length = 255)
    private String WebServerRedirectUri;
  
    @Column(name = "authorities", length = 255)
    private String authorities;
    
    @Column(name = "access_token_validity")
    private int AccessTokenValidity;
   
    @Column(name = "refresh_token_validity")
    private int RefreshTokenValidity;
    
    @Column(name = "additional_information", length = 4096)
    private String AdditionalInformation;
   
    @Column(name = "autoapprove", length = 255)
    private String autoApprove;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAuthorizedGrantTypes() {
        return AuthorizedGrantTypes;
    }

    public void setAuthorizedGrantTypes(String AuthorizedGrantTypes) {
        this.AuthorizedGrantTypes = AuthorizedGrantTypes;
    }

    public String getWebServerRedirectUri() {
        return WebServerRedirectUri;
    }

    public void setWebServerRedirectUri(String WebServerRedirectUri) {
        this.WebServerRedirectUri = WebServerRedirectUri;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public int getAccessTokenValidity() {
        return AccessTokenValidity;
    }

    public void setAccessTokenValidity(int AccessTokenValidity) {
        this.AccessTokenValidity = AccessTokenValidity;
    }

    public int getRefreshTokenValidity() {
        return RefreshTokenValidity;
    }

    public void setRefreshTokenValidity(int RefreshTokenValidity) {
        this.RefreshTokenValidity = RefreshTokenValidity;
    }

    public String getAdditionalInformation() {
        return AdditionalInformation;
    }

    public void setAdditionalInformation(String AdditionalInformation) {
        this.AdditionalInformation = AdditionalInformation;
    }

    public String getAutoApprove() {
        return autoApprove;
    }

    public void setAutoApprove(String autoApprove) {
        this.autoApprove = autoApprove;
    }
    
    

}
